import 'package:archive/archive.dart';
import 'dart:io';

class ZipToolService {
  static ZipToolService _zipToolService;

  static ZipToolService getInstance() {
    init();
    return _zipToolService;
  }

  static void init() {
    if (_zipToolService == null) {
      _zipToolService = new ZipToolService();
    }
  }

  ZipToolService();

  Future<List<String>> unarchiveAndSave(var zippedFile, String _dir) async {
    List<String> _filePath = [];
    var bytes = zippedFile.readAsBytesSync();
    var archive = ZipDecoder().decodeBytes(bytes);

    for (var file in archive) {
      var fileName = '$_dir/${file.name}';
      if (file.isFile) {
        var outFile = File(fileName);
        print('File:: ' + outFile.path);
        _filePath.add(outFile.path);
        outFile = await outFile.create(recursive: true);
        await outFile.writeAsBytes(file.content);
      }
    }
    return _filePath;
  }
}
