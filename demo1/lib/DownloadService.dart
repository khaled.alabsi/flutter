import 'dart:io';
import 'package:flutter/foundation.dart';

class DownloadService {

  String msg = "";
  HttpClient httpClient;
  bool isDownloading = false;
  bool hasError = false;
  static DownloadService _downloadService;

  DownloadService() {
    httpClient = new HttpClient();
  }

  static DownloadService getInstance() {
    init();
    return _downloadService;
  }

  static void init() {
    if (_downloadService == null) {
      _downloadService = new DownloadService();
    }
  }


  Future<File> downloadFile(_dir, String url, String fileName) async {
/*    if (null == _dir) {
      _dir = (await getApplicationDocumentsDirectory()).path;
      print(_dir);
    }*/
    File file;
    String filePath = '';
    String myUrl = '';

    try {
      myUrl = url + '/' + fileName;
      isDownloading = true;
      var request = await httpClient.getUrl(Uri.parse(url));
      var response = await request.close();
      if (response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = '$_dir/$fileName';
        file = File(filePath);

        isDownloading = false;
        msg = "done downloading";
        return await file.writeAsBytes(bytes);
      } else
        hasError = true;
      msg = 'Error code: ' + response.statusCode.toString();
    } catch (ex) {
      {
        hasError = true;
        msg = 'Can not fetch url';
        isDownloading = false;
      }
    }
  }
}
