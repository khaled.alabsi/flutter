import 'package:flutter/material.dart';
import 'dart:io';
import 'package:archive/archive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:demo1/DownloadService.dart';
import 'package:demo1/ZipToolService.dart';

void main() => runApp(MaterialApp(home: DownloadAssetsDemo()));

class DownloadAssetsDemo extends StatefulWidget {
  DownloadAssetsDemo() : super();

  final String title = "Download & Extract ZIP Demo";

  @override
  DownloadAssetsDemoState createState() => DownloadAssetsDemoState();
}

class DownloadAssetsDemoState extends State<DownloadAssetsDemo> {
  //
  bool _downloading;
  String _dir;
  List<String> _images, _tempImages;

  //String _zipPath = 'https://coderzheaven.com/youtube_flutter/images.zip';
  String _zipPath =
      'https://github.com/khaledpage/data/archive/refs/heads/main.zip';

  //https://github.com/khaledpage/data/archive/refs/heads/2branch.zip
  String _localZipFileName = 'images.zip';
  String _msg = "nun";
  DownloadService downloadService= DownloadService.getInstance();
  ZipToolService zipToolService= ZipToolService.getInstance();

  @override
  void initState() {
    super.initState();
    _images = [];
    _tempImages = [];
    _downloading = false;

    _initDir();
  }

  _initDir() async {
    if (null == _dir) {
      _dir = (await getApplicationDocumentsDirectory()).path;
      print(_dir);
    }
  }

  Future<void> _downloadZip() async {
    setState(() {
      _downloading = true;
    });

    _images.clear();
    _tempImages.clear();

    var zippedFile =
        await downloadService.downloadFile(_dir, _zipPath, _localZipFileName);
    if (!downloadService.isDownloading) {
      setState(() {
        _downloading = false;
      });
    }
    _tempImages= await zipToolService.unarchiveAndSave(zippedFile,_dir);

    setState(() {
      _images.addAll(_tempImages);
    });
  }

  // unarchiveAndSave(var zippedFile) async {
  //   var bytes = zippedFile.readAsBytesSync();
  //   var archive = ZipDecoder().decodeBytes(bytes);
  //   for (var file in archive) {
  //     var fileName = '$_dir/${file.name}';
  //     if (file.isFile) {
  //       var outFile = File(fileName);
  //       print('File:: ' + outFile.path);
  //       _tempImages.add(outFile.path);
  //       outFile = await outFile.create(recursive: true);
  //       await outFile.writeAsBytes(file.content);
  //     }
  //   }
  // }

  buildList() {
    return Expanded(
      child: ListView.builder(
        itemCount: _images.length,
        itemBuilder: (BuildContext context, int index) {
          return Image.file(
            File(_images[index]),
            fit: BoxFit.fitWidth,
          );
        },
      ),
    );
  }

  progress() {
    return Container(
      width: 25,
      height: 25,
      padding: EdgeInsets.fromLTRB(0.0, 20.0, 10.0, 20.0),
      child: CircularProgressIndicator(
        strokeWidth: 3.0,
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          _downloading ? progress() : Container(),
          IconButton(
            icon: Icon(Icons.file_download),
            onPressed: () {
              _downloadZip();
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Text(
              "haööp",
              style: TextStyle(color: Colors.black, fontSize: 20.0),
            ),
            Text(
              _msg,
              style: TextStyle(color: Colors.black, fontSize: 20.0),
            ),
            buildList(),
          ],
        ),
      ),
    );
  }
}
